import * as photoloader from './photoloader.js';

let vignette = '';
let image = '';

export function init(e){
	$('#lightbox_close').on("click",() =>{
		image.remove();
		$('#lightbox_container').css("display","none");
	});

  $('#lightbox-prev').on("click",() =>{
    if(vignette.previousElementSibling==null){
      vignette=$("#photobox-gallery")[0].lastElementChild;
    }
    else vignette = vignette.previousElementSibling;
    let img = vignette.firstElementChild.getAttribute("data-img");
    let titre = vignette.textContent;
    image.remove();
    image = $('<img src="'+img+'">');
    $('#lightbox-img').append(image);
    $('#lightbox_title').html(titre);
    photoloader.loadInfo(vignette.getAttribute("data-id"));
  });

  $('#lightbox-next').on("click",() =>{
    if(vignette.nextElementSibling==null){
      vignette = $("#photobox-gallery")[0].firstElementChild;
    }
    else vignette = vignette.nextElementSibling;
    let img = vignette.firstElementChild.getAttribute("data-img");
    let titre = vignette.textContent;
    image.remove();
    image = $('<img src="'+img+'">');
    $('#lightbox-img').append(image);
    $('#lightbox_title').html(titre);
    photoloader.loadInfo(vignette.getAttribute("data-id"));
  });
}

export function afficherImage(e){
	vignette = e.currentTarget;
	let img = vignette.firstElementChild.getAttribute("data-img");
	let titre = vignette.textContent;
  image = $('<img id="lightbox_full_img" src="'+img+'">');
	$('#lightbox-img').append(image);
	$('#lightbox_title').html(titre);
	$('#lightbox_container').css("display","block");
	photoloader.loadInfo(vignette.getAttribute("data-id"));
}

export function informations(d){
			$('#lightbox_info').empty();
			let info = $('<br><h3>Informations :</h3><p><strong>Taille :</strong><br>'+d.width+'x'+d.height+' px<br><strong>Format :</strong><br>'+d.format+'<br><strong>Poids :</strong><br>'+Math.round(d.size/1024)+' Ko<p>');
			$('#lightbox_info').append($('<h3>Description :<h3>'));
			$('#lightbox_info').append($('<p>'+d.descr+'</p>'));
			$('#lightbox_info').append(info);
		}
