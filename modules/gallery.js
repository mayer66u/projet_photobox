import * as photoloader from './photoloader.js';
import * as lightbox from './lightbox.js';

export function init(){
  $('#load_gallery').on("click",  () => {
                                chargement();
                                document.querySelector('#next').removeAttribute("disabled");
                                document.querySelector('#previous').removeAttribute("disabled");
                            });
}

function chargement(){
  photoloader.chargementListeObjets("/www/canals5/photobox/photos/?offset=8&size=12");
}

function inserer(objet){
  let photo=objet.photo;
	let srvAdress = photoloader.getAdresseServeur();
	let thumbnail =srvAdress+photo.thumbnail.href;
	let original = srvAdress+photo.original.href;
	let titre = photo.titre;
	let vignette = $('<div class="vignette" data-id="'+objet.links.self.href+'"><img data-img="'+original+'" src="'+thumbnail+'"> <div>'+titre+'</div></div>');
	$('#photobox-gallery').append(vignette);
	vignette.on("click",(e) => { lightbox.afficherImage(e) });
}

export function afficherPhotos(d){
  $('#photobox-gallery').empty();
	d.forEach(inserer);
}
