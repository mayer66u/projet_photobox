import * as gallery from './gallery.js';
import * as lightbox from './lightbox.js';

let adresseServeur = '';
let liens = '';
let url = '';

export function init(server_url){
  adresseServeur=server_url;
  gallery.init();
  $('#next').on("click",() => { pageSuivante() });
  $('#previous').on("click",() => { pagePrecedente() });
  document.querySelector('#next').setAttribute("disabled", false);
  document.querySelector('#previous').setAttribute("disabled", false);
  lightbox.init();
}

export function chargementListeObjets(uri){
  url = uri;
  let pr = $.ajax(adresseServeur+uri, {
				type: 'GET',
				dataType: 'json',
				xhrFields: {
					withCredentials: true
				}
			});
	pr.fail(function(d,s,x){
				alert("ajax call failed : code " + s);
	});
	pr.done(function(d,s,x){
				liens=d.links;
				gallery.afficherPhotos(d.photos);
	});

  /*let pr = axios.get(adresseServeur+uri)
    	.catch(function(d,s,x){
    		alert("ajax call failed : code " + s);
    	})
    	.then(function(d,s,x){
    		liens=d.links;
    		gallery.afficherPhotos(d.photos);
    	});*/

	return pr;
}

export function getAdresseServeur(){
  return adresseServeur;
}

function pageSuivante(){
	let suiv = liens.next.href;
	if(url != suiv){
    chargementListeObjets(suiv);
  }
}

function pagePrecedente(){
	let prec = liens.prev.href;
	if(url != prec){
    chargementListeObjets(prec);
  }
}

export function loadInfo(uri){
	 var pr = $.ajax(adresseServeur+uri, {
	    type: 'GET',
	     dataType: 'json',
       xhrFields: {
	        withCredentials: true
	     }
	 });
	 pr.fail(function(d,s,x){
			alert("ajax call failed : code" + s);
	 });
	 pr.done(function(d,s,x){
			lightbox.informations(d.photo);
	 });
	 return pr;
}
